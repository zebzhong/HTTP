const http = require('http')
const fs = require('fs')

const app = http.createServer((req, res) => {
  console.log("req come：" + req.url);

  const html = fs.readFileSync('test.html', 'utf8')
  const img = fs.readFileSync('test2.jpg')

  if(req.url === '/') {
    res.writeHead(200, {
      'Content-Type': 'text/html',
      // http2 服务端推送什么文件
      'Link': '</test2.jpg>;as=image;rel=preload'
    })
    res.end(html)
  } else {
    res.writeHead(200, {
      'Content-Type': 'image/jpg'
    })
    res.end(img)
  }
})

app.listen(3000)