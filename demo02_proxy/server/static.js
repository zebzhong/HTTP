const path = require("path");
const Koa = require("koa");
const app = new Koa();
const jkyunRouter = require("koa-router")();
const zbRouter = require("koa-router")();
const testRouter = require("koa-router")();
const { createProxyMiddleware } = require("http-proxy-middleware");
const proxy = require("http-proxy-middleware");
const k2c = require("koa2-connect");

app.use(require("koa-static")(path.resolve(__dirname, "../static")));

const options = {
  target: "http://localhost:3000", // target host
  changeOrigin: true, // needed for virtual hosted sites
  pathRewrite: {
    "^/jkyun/api": "/api", // rewrite path
    "^/zb/api": "/api",
  },
};
const options2 = {
  target: "http://localhost:3001",
  changeOrigin: true,
  pathRewrite: {
    "^/test/api": "/api",
  },
};
// 第一种传统方式
// app.use(async (ctx, next) => {
//   let url = ctx.request.url;
//   if(url.startsWith('/jkyun/api') || url.startsWith('/zb/api')) {
//     ctx.respond = false;
//     await k2c(createProxyMiddleware(options))(ctx, next);
//   } else if(url.startsWith('/test/api')) {
//     ctx.respond = false;
//     await k2c(createProxyMiddleware(options2))(ctx, next);
//   } else {
//     await next();
//   }
// })

// 第二种针对路由方式
// jkyunRouter.prefix('/jkyun/api')

// jkyunRouter.all('/(.*)', async (ctx, next) => {
//   ctx.respond = false;
//   await k2c(createProxyMiddleware(options))(ctx, next);
// });

// app.use(jkyunRouter.routes(), jkyunRouter.allowedMethods());

// zbRouter.prefix('/zb/api')

// zbRouter.all('/(.*)',async (ctx, next) => {
//   ctx.respond = false;
//   await k2c(createProxyMiddleware(options))(ctx, next);
// });

// app.use(zbRouter.routes());

// testRouter.prefix('/test/api')

// testRouter.all('/(.*)',async (ctx, next) => {
//   ctx.respond = false;
//   await k2c(createProxyMiddleware(options2))(ctx, next);
// });

// app.use(testRouter.routes());

// 第三种方式
const options3 = {
  "/jkyun/api": {
    target: "http://localhost:3000", // target host
    changeOrigin: true, // needed for virtual hosted sites
    pathRewrite: {
      "^/jkyun/api": "/api", // rewrite path
      "^/zb/api": "/api",
    },
  },
  "/zb/api": {
    target: "http://localhost:3000", // target host
    changeOrigin: true, // needed for virtual hosted sites
    pathRewrite: {
      "^/zb/api": "/api",
    },
  },
  "/test/api": {
    target: "http://localhost:3001",
    changeOrigin: true,
    pathRewrite: {
      "^/test/api": "/api",
    },
  }
};

// app.use(k2c(proxy(options3)))
app.use(async(ctx, next) => {
  Object.keys(options3).forEach(proxyKey => {
    if(ctx.url.startsWith(proxyKey)) {
      ctx.respond = false;
      k2c(createProxyMiddleware(options3[proxyKey]))(ctx, next);
    }
  })
})

app.listen(4000, () => {
  console.log(`4000端口启动`);
});
