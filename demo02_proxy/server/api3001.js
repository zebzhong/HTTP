const Koa = require('koa')
const router = require('koa-router')()
const app = new Koa()

router.get('/api/user', async (ctx, next) => {
  ctx.body = {
    name: 'test',
    age: 986
  }
});

app.use(router.routes(), router.allowedMethods());

app.listen(3001, () => {
  console.log(`3001端口启动`);
})