const Koa = require('koa')
const router = require('koa-router')()
const app = new Koa()

router.get('/api/user', async (ctx, next) => {
  ctx.body = {
    name: 'jkyun',
    age: 18
  }
});

router.get('/api/zb', async (ctx, next) => {
  ctx.body = {
    name: 'zb',
    age: 32
  }
});

app.use(router.routes(), router.allowedMethods());

app.listen(3000, () => {
  console.log(`3000端口启动`);
})