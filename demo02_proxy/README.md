# 1.添加代理，不设置代理启动访问报错

```
const { createProxyMiddleware } = require('http-proxy-middleware');
const k2c = require('koa2-connect');

const options = {
  target: 'http://localhost:3000', // target host
  changeOrigin: true, // needed for virtual hosted sites
  pathRewrite: {
    '^/jkyun/api': '/api', // rewrite path
  }
};

app.use(async(ctx, next) => {
  ctx.respond = false;
  await k2c(createProxyMiddleware(options))(ctx, next);
})

```