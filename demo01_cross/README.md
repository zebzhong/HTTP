
## 1.启动：

```
  1. cd demo01_cross/server && nodemon index.js

  2. 界面输入 http://localhost:4000

  说明：4000端口启动html的访问，3000端口启动server
```

### 2.测试跨域

1. 在4000端口的页面中发出对3000端口的请求，

2. 在index.html里面配置axios.defaults.baseURL = "http://localhost:3000"，同时在4000端口的页面打印请求，可以发现，服务器有响应，但是浏览器未接受数据

3. 此现象说明==跨域是存在于浏览器端==，服务器能接收，同时查看浏览器，会发现有同源限制

### 3.可以引入cors中间件，服务器设置Access-Control-Allow-Origin：4000，即开放3000端口过来的访问，可以发现此时界面能正常看到返回的数据了

### 4.如果在index.html发出的请求上加上headers: {'X-Token': 'jfsda'}，会发现页面会多发一个请求，请求的method是OPTIONS，专业术语称为预检请求，参考链接： <https://www.jianshu.com/p/b55086cbd9af>，如果要解决此问题，则设置

```
const cors = require('koa2-cors')

app.use(cors({
  // 第二步 防止不同源即 Access-Control-Allow-Origin
  origin: function (ctx) {
      return 'http://localhost:4000';
  },
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  // 第三步，防止预检 即 Access-Control-Allow-Headers， Access-Control-Allow-Methods
  allowMethods: ['GET', 'POST', 'DELETE', 'PUT'], //设置允许的HTTP请求类型
  allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'X-Token'],
  
}));
```

### 5.如果要携带cookie信息，则请求变为credential,需要设置Access-Control-Allow-Credentials为true
```
app.use(cors({
  ...
  credentials: true
}))

```