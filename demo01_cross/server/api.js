const Koa = require('koa')
const router = require('koa-router')()
const cors = require('koa2-cors')
const app = new Koa()

app.use(cors({
  // 第二步 防止不同源即 Access-Control-Allow-Origin
  origin: function (ctx) {
      return 'http://localhost:4000';
  },
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  // 第三步，防止预检 即 Access-Control-Allow-Headers， Access-Control-Allow-Methods
  allowMethods: ['GET', 'POST', 'DELETE', 'PUT'], //设置允许的HTTP请求类型
  allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'X-Token'],
  credentials: true
}));

router.get('/api/user', async (ctx, next) => {
  ctx.body = {
    name: 'zb',
    age: 18
  }
});

app.use(router.routes(), router.allowedMethods());

app.listen(3000, () => {
  console.log(`3000端口启动`);
})