const path = require('path')
const Koa = require('koa')
const app = new Koa()
const { createProxyMiddleware } = require('http-proxy-middleware');
const k2c = require('koa2-connect');
// 6.设置代理转发

app.use(require('koa-static')(path.resolve(__dirname, "../static")));

app.listen(4000, () => {
  console.log(`4000端口启动`);
})