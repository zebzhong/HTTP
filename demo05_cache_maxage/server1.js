const fs = require('fs')
const http = require('http')

const app = http.createServer((req, res) => {
  console.log(req.url);

  if(req.url === '/') {
    const html = fs.readFileSync('test.html', 'utf8')
    res.writeHead(200, {
      'Content-Type': 'text/html'
    })
    res.end(html)
  }

  if(req.url === '/script.js') {
    const etag = req.headers['if-none-match']
    if(etag === '777') {
      res.writeHead(304, {
        'Content-Type': 'text/javascript',
        // 设置了no-cache，则表示依然会发起服务端验证，而不受max-age影响，但是如果设置了no-store，则表示一定会从服务器拿资源，表示这个是全新的请求
        'Cache-Control': 'max-age=2000000, no-cache',
        'Last-Modified': '123',
        'Etag': '777'
      })
      res.end('')
    } else {
      res.writeHead(200, {
        'Content-Type': 'text/javascript',
        // 设置了no-cache，则表示依然会发起服务端验证，而不受max-age影响
        'Cache-Control': 'max-age=2000000, no-cache',
        'Last-Modified': '123',
        'Etag': '777'
      })
      res.end('console.log("script end twice")');
    } 
  }

  if(req.url === 'favicon.ico') {
    res.end('')
  }
})

app.listen(3000)