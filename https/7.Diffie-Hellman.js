const { createDiffieHellman } = require('crypto');
// 客户端
var client = createDiffieHellman(512);
var client_keys = client.generateKeys(); //A

var prime = client.getPrime(); // P
var generator = client.getGenerator(); // N

// 服务端
var server = createDiffieHellman(prime, generator);
var server_keys = server.generateKeys();

var client_secret = client.computeSecret(server_keys);
var server_secret = server.computeSecret(client_keys);

console.log('client_secret: ' + client_secret.toString('hex'));
console.log('server_secret: ' + server_secret.toString('hex'));

// 原理
// let N = 23;
// let p = 5;
// let secret1 = 6;// 这是密钥
// let A = Math.pow(p, secret1) % N;// 8
// console.log('p=', p, 'N=', N, 'A=', A);

// let secret2 = 15;
// let B = Math.pow(p, secret2) % N;// 19
// console.log('p=', p, 'N=', N, 'B=', B);

// console.log(Math.pow(B, secret1) % N);
// console.log(Math.pow(A, secret2) % N);