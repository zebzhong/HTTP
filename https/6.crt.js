let { generateKeyPairSync, createSign, createVerify, createHash } = require('crypto');
let passphrase = 'zbtest'
let serverRSA = generateKeyPairSync('rsa', {
    modulusLength: 1024,
    publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
    },
    privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
        cipher: 'aes-256-cbc',
        passphrase // 私钥的密码
    }
});
// console.log('serverRSA', serverRSA)
let caRSA = generateKeyPairSync('rsa', {
  modulusLength: 1024,
  publicKeyEncoding: {
      type: 'spki',
      format: 'pem'
  },
  privateKeyEncoding: {
      type: 'pkcs8',
      format: 'pem',
      cipher: 'aes-256-cbc',
      passphrase // 私钥的密码
  }
});
// console.log('caRSA', caRSA)
const info = {
  domain: 'http://127.0.0.1:8080',
  publicKey: serverRSA.publicKey
}
// 把这个申请信息发给CA机构请求颁发证书
// 实现签名的时候不是真正的info，而是它的hash，因为签名性能很差，一般不能计算大量的数据
let sha256 = createHash('sha256');
let caHash = sha256.update(JSON.stringify(info)).digest('hex');
let sign = getSign(caHash, caRSA.privateKey, passphrase);
let cert = {
  info,
  sign //CA的签名
} // 这就是证书，客户端会先验证书的合法性，用CA的公钥验证证书的合法性，然后取出公钥
function getSign(hash, privateKey, passphrase) {
  let sign = createSign('RSA-SHA256');
  sign.update(hash);
  return sign.sign({ key: privateKey, format: 'pem', passphrase }, 'hex');
}
function verifySign(content, sign, publicKey) {
  var verify = createVerify('RSA-SHA256');
  verify.update(content);
  return verify.verify(publicKey, sign, 'hex');
}

let caIsValid = verifySign(caHash, cert.sign, caRSA.publicKey);
console.log('caIsValid', caIsValid)