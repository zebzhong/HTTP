let crypto = require('crypto')
// 对称加密
function encrypt(data, key, iv){
  let cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
  cipher.update(data);
  return cipher.final('hex'); // 十六进制，把结果输出成十六进制的字符串
}

function decrypt(data, key, iv) {
  let cipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
  cipher.update(data, 'hex');
  return cipher.final('utf-8') // 输出成utf8字符
}

let msg = 'hello';
// 如果加密算法是128，则对应的密钥是16位，加密算法是256，则对应的密钥是32位
// let key = '0123456789123456'
// let iv = '0123456789123456'
let key = crypto.randomBytes(16);
let iv = crypto.randomBytes(16);
let encryptMsg = encrypt(msg, key, iv);
let decryptMsg = decrypt(encryptMsg, key, iv);
console.log('encryptMsg', encryptMsg)
console.log('decryptMsg', decryptMsg)