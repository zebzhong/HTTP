// 非对称加密
// 加密的密钥和解密的密钥不一样
// 但是他们有关系，不能通过公钥算出密钥
// 两个质数相乘得到一个结果 正向乘很容易，但是给你一个乘积，不知道他们是由哪两个数乘出来的

let p = 3, q = 11;
let N = p * q; // 33
let fN = (p - 1) * (q - 1); // 欧拉函数
let e = 7; // 随意挑一个数
// {e, N} 就是我们的公钥，公钥可以发给任何人，是完全公开的
// 可以从公钥推算出私钥，但是得知道fN
for (var d = 1; e * d % fN !== 1; d++) {//拓展欧几里得算法
  d++;
}
//d=3
let publicKey = { e, N };
let privateKey = { d, N };

function encrypt(data) {
  return Math.pow(data, publicKey.e) % publicKey.N;
}
function decrypt(data) {
  return Math.pow(data, privateKey.d) % privateKey.N;
}
let data = 5;
let secret = encrypt(data);
console.log(secret);//14

let _data = decrypt(secret);
console.log(_data);//5