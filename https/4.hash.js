let crypto = require('crypto');

let msg = 'hello';
let md5 = crypto.createHash('md5');
let md5Str = md5.update(msg).digest('hex');
console.log('md5Str', md5Str, md5Str.length)

let salt = '123456'
let sha256 = crypto.createHmac('sha256', salt);
let sha256Str = sha256.update(msg).digest('hex');
console.log('sha256Str', sha256Str, sha256Str.length)

let sha256Hash = crypto.createHash('sha256');
let sha256HashStr = sha256Hash.update('zbtest').digest('hex');
console.log(sha256HashStr, sha256HashStr.length);