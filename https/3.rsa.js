let { generateKeyPairSync, privateEncrypt, publicDecrypt } = require('crypto');

let rsa = generateKeyPairSync('rsa', {
  modulusLength: 1024,
  publicKeyEncoding: {
    type: 'spki',
    format: 'pem' // base64格式的私钥
  },
  privateKeyEncoding: {
    type: 'pkcs8',
    format: 'pem',
    cipher: 'aes-256-cbc', // 加密方式
    passphrase: 'server_passphrase' // 密码
  }
})
// console.log('rsa', rsa)
let msg = 'hello';

let enc_by_prv = privateEncrypt({
  key: rsa.privateKey, 
  passphrase: 'server_passphrase'
}, Buffer.from(msg, 'utf8'));
console.log('encrypted by private key: ' + enc_by_prv.toString('hex'));


let dec_by_pub = publicDecrypt(rsa.publicKey, enc_by_prv);
console.log('decrypted by public key: ' + dec_by_pub.toString('utf8'));