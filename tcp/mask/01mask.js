// 根据IP计算是否在同一个网段内，能否互相通信
let ip1 = '100.50.20.2';
let ip2 = '100.60.20.2';
let mask = '255.0.0.0'; // 子网掩码
function getBinaryForIp(ip) {
  return ip.split('.').map(item => parseInt(item).toString(2).padStart(8, '0')).join('');
}
function isSameNet(ip1, ip2, mask){
  let ip1Binary = getBinaryForIp(ip1);
  let ip2Binary = getBinaryForIp(ip2);
  let maskBinary = getBinaryForIp(mask);
  let num1 = parseInt(ip1Binary, 2) & parseInt(maskBinary, 2);
  let num2 = parseInt(ip2Binary, 2) & parseInt(maskBinary, 2);
  console.log('num1 === num2', num1 === num2)
  return num1 === num2;
}
isSameNet(ip1, ip2, mask);