const net = require('net');
let server = net.createServer(function(socket) {
  socket.on('data', function(data) {
    console.log('server data', data.toString())
    socket.write(data)
  })
  socket.on('end', function(data) {
    console.log('end', data)
  })
  socket.on('error', function(error) {
    console.log('server error', error)
  })
})
server.listen(8080, () => {
  console.log('server started at 8080 port');
})
server.on("close", function () {
  console.log("server closed!");
})