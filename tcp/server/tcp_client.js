let net = require('net');
let socket = net.Socket();
socket.connect(8080, 'localhost');
socket.on('connect', function(){
  socket.write('hello');
})
socket.on('data', function(data){
  console.log('client data', data.toString())
})
socket.on('error', function(error) {
  console.log('client error', error)
})
socket.on("end", function () {
  console.log("client data end");
})