const fs = require('fs');
const { RSA_NO_PADDING } = require('constants');

function get(key) {
  fs.readFile('../json/db.json', (error, data) => {
    if(error) {
      console.log(error);
      return;
    }
    const json = JSON.parse(data);
    console.log(json[key]);
  })
}

function set(key, value) {
  fs.readFile('../json/db.json', (error, data) => {
    const json = data ? JSON.parse(data) : {};
    json[key] = value
    fs.writeFile('../json/db.json', JSON.stringify(json), (err, data) => {
      if(err) {
        console.log(err);
      } else {
        console.log("写入成功");
      }
    })
  })
}

const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.output
})

rl.on('line', (input) => {
  const [op, key, value] = input.split(" ")
  if(op === 'get') {
    get(key)
  } else if(op === 'set') {
    set(key, value)
  } else if(op === 'quit') {
    rl.close();
  } else {
    console.log("no operation");
  }
})

rl.on('close', (err) => {
  
})