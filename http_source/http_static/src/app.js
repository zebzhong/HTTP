let http = require('http')
let chalk = require('chalk');
let path = require('path');
let url = require('url')
let fs = require('fs')
let util = require('util')
let mime = require('mime');
let Handlebars = require('handlebars')
let crypto = require('crypto')
let zlib = require('zlib');
// 这是一个在控制台输出得模块，名称有特点两部分，一般第一部分是项目名，第二部分是模块名
// 每个debug实例都有一个名字，是否在控制台打印取决于环境变量中DEBUG得值是否等于static:app
// windows下在cmd中输入 set DEBUG=static:app linux mac下在shell中输入export DEBUG=static:app
let debug = require('debug')('static:app'); 

const fsStat = util.promisify(fs.stat)
const readFileSync = fs.readFileSync
const fsReaddir = util.promisify(fs.readdir)

let config = require('./config')

function list(){
  let tpl = fs.readFileSync(path.resolve(__dirname, 'tpl', 'list.html'), 'utf-8');
  return Handlebars.compile(tpl);
}

class Server {
  constructor(argv = {}){
    this.template = list();
    this.config = Object.assign({}, config, argv)
    console.log('this.config', this.config)
  }
  start(){
    let server = http.createServer();
    server.on('request', this.request.bind(this));
    server.listen(this.config.port, () => {
      let url =  `${this.config.host}:${this.config.port}`;
      debug(`server started at ${chalk.green(url)}`);      
    })
  }
  // 静态服务
  async request(req, res){
    let { pathname } = url.parse(req.url, true);
    pathname = decodeURIComponent(pathname)
    let filepath = path.join(this.config.root, pathname);
    try {
      let statObj = await fsStat(filepath);
      if (statObj.isDirectory()) {
        let files = await fsReaddir(filepath);
        files = files.map(file => ({
          name: file,
          url: path.join(pathname, file)
        }))
        let html = this.template({
          title: pathname,
          files
        })
        res.setHeader('Content-Type', 'text/html;charset=utf-8');
        res.end(html)
      } else {
        this.sendFile(req, res, filepath, statObj)
      }
    } catch (error) {
      debug(util.inspect(error)); //inspect把一个对象转换成字符串
      this.sendError(req, res)
    }
  }
  sendError(req, res) {
    res.statusCode = 404;
    res.end(`there is something wrong in the server! please try later!`);
  }
  handleCache(statObj, filePath, req, res){
    res.setHeader('Expires', new Date(Date.now() + 10*1000).toGMTString());

    res.setHeader('Cache-Control', 'max-age=10');

    let ifModifiedSince = req.headers['if-modified-since'];
    
    let ifNoneMatch = req.headers['if-none-match'];

    let ctime = statObj.ctime;
    let md5 = crypto.createHash('md5');
    // 当文件小时
    let ETag = md5.update(readFileSync(filePath, 'utf-8')).digest('base64');
    // 当文件大时，使用流
    // let outStream = fs.createReadStream(filePath);
    // outStream.on('data', function(data) {
    //   md5.update(data);
    // })
    // outStream.on('end', function() {
    //   let Etag = md5.digest('hex');
    //   // ....
    // })
    res.setHeader('Last-Modified', ctime);
    res.setHeader('ETag', ETag);

    if(ifModifiedSince != ctime) {
      return false;
    }

    if(ifNoneMatch != ETag) {
      return false;
    }

    return true;
  }
  handleZip(req, res, inputStream){
    const acceptEncoding = req.headers['accept-encoding'] || '';
    if(acceptEncoding.match(/\bdeflate\b/)) {
      res.setHeader('Content-Encoding', 'deflate'); // 如果不设置，则浏览器会乱码
      inputStream.pipe(zlib.createDeflate()).pipe(res);
    } else if(acceptEncoding.match(/\bgzip\b/)) {
      res.setHeader('Content-Encoding', 'gzip');
      inputStream.pipe(zlib.createGzip()).pipe(res);
    } else {
      inputStream.pipe(res);
    }
  }
  sendFile(req, res, filepath, statObj) {
    if(this.handleCache(statObj, filepath, req, res)) {
      res.statusCode = 304;
      res.end();
      return;
    }
    res.setHeader('Content-Type', mime.getType(filepath)+';charset=utf-8'); // 如果不添加charset=utf-8，则返回的html页面会显示乱码
    let inputStream = fs.createReadStream(filepath);
    this.handleZip(req, res, inputStream);
  }
}

module.exports = Server