// -d --root 静态文件目录
// -o --host 主机
// -p --port 端口号

const { Command } = require("commander");
const program = new Command();
const config = require("../src/config");

let Server = require('../src/app')

program
  .version(require("../package.json").version)
  .usage("zb-server [options]")
  .description("An application for static app")
  .option("-d, --root <string>", "静态文件根目录", config.root)
  .option("-o, --host <string>", "请配置监听的主机", config.host)
  .option("-p, --port <number>", "请配置监听的端口号", config.port);
program.on("--help", function () {
  console.log("");
  console.log("Examples:");
  console.log("  $ zb-server --help");
  console.log("  $ zb-server -h");
});
program.parse(process.argv);

let server = new Server(program.args);
server.start();
