let http = require('http');
let url = require('url')
let zlib = require('zlib');
let fs = require('fs');
let path = require('path')
let mime = require('mime')
let { promisify } = require('util')
let fsStat = promisify(fs.stat);

const server = http.createServer(async (req, res) => {
  let headers = req.headers;
  let { pathname } = url.parse(req.url)
  let filepath = path.join(__dirname, pathname);
  try {
    await fsStat(filepath);
    let acceptEncoding = headers['accept-encoding'] || '';
    let inputStream = fs.createReadStream(filepath);
    res.setHeader('Content-Type', mime.getType(pathname))
    console.log('filepath', filepath)
    if(acceptEncoding.match(/\bdeflate\b/)){
      res.setHeader('Content-Encoding','deflate');
      inputStream.pipe(zlib.createDeflate()).pipe(res);
    } else if(acceptEncoding.match(/\bgzip\b/)) {
      res.setHeader('Content-Encoding','gzip')
      inputStream.pipe(zlib.createGzip()).pipe(res)
    } else {
      inputStream.pipe(res);
    }
  } catch (error) {
    res.statusCode = 404;
    res.end();
  }
})

server.listen(3000)