const fs = require('fs')
const path = require('path')
const zlib = require('zlib')

function gzip(src) {
  return fs.createReadStream(src).pipe(zlib.createGzip()).pipe(fs.createWriteStream(src + '.gz'))
}
// basename 从一个路径中得到文件名，包括扩展名的，可以传一个扩展名字符串参数去掉扩展名
// extname 获取扩展名
function unGzip(src) {
  let originalFilename = path.basename(src, '.gz');
  return fs.createReadStream(src).pipe(zlib.createGunzip()).pipe(fs.createWriteStream(path.resolve(__dirname, originalFilename)))
}

// gzip(path.resolve(__dirname, './msg.txt'))
unGzip(path.resolve(__dirname, './msg.txt.gz'))