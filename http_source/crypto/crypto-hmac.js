let crypto = require('crypto')
let path = require('path');
let fs = require('fs');

// openssl genrsa -out rsa_private.key 1024 生成密钥，即加盐
let pem = fs.readFileSync(path.join(__dirname, './rsa_private.key'));
let key = pem.toString('ascii')
console.log('key', key)
let hmac = crypto.createHmac('sha1', key);

let result = hmac.update('hello').digest('hex');
console.log('result', result)