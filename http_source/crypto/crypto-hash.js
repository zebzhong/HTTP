let crypto = require('crypto');

let str = 'hello';
let str2 = 'world';

// console.log('crypto.getHashes()', crypto.getHashes())
let md5 = crypto.createHash('md5');
let sha1 = crypto.createHash('sha1');

let md5Str = md5.update(str).update(str2).digest('hex'); // 输出md5值，指定输出的格式 hex 十六进制
let sha1Str = sha1.update(str).update(str2).digest('hex'); //
console.log('md5Str', md5Str) // fc5e038d38a57032085441e7fe7010b0
console.log('sha1Str', sha1Str) // 6adfb183a4a2c94a2f92dab5ade762a47889a5a1