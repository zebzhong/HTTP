const url = require('url');
let str = 'http://www.baidu.com:3000/user?id=54&id=hello#top';
let urlObj = url.parse(str, true);
console.log('urlObj', urlObj)
// urlObj Url {
//   protocol: 'http:',  //协议
//   slashes: true, // 表示http:后面的//
//   auth: null, // 权限
//   host: 'www.baidu.com:3000', // host地址
//   port: '3000', // 端口号
//   hostname: 'www.baidu.com', // 域名
//   hash: '#top', // hash
//   search: '?id=54', // search参数
//   query: 'id=54', // 查询参数
//   pathname: '/user', // pathname
//   path: '/user?id=54', // 完整path
//   href: 'http://www.baidu.com:3000/user?id=54#top'
// }
