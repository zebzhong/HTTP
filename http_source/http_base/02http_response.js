const http = require('http')

const server = http.createServer((req, res) => {
  // res.statusCode = 200 // 设置响应码
  // res.statusMessage = 'not found' // 设置返回响应码后面的文本
  // res.sendDate = false; // 不发送日期
  // res.setHeader('Content-Type', 'text/html;charset=utf8');
  // let header = res.getHeader('Content-Type');
  // let allHeaders = res.getHeaders();
  // // res.removeHeader()
  // console.log('header', header)
  // console.log('allHeaders', allHeaders)  
  // res.write('hello');
  // res.write('world');
  // res.end();

  // 在同一个方法里设置状态码，原因短语，响应头
  // writeHead一旦调用会立刻向客户端发送，setHeader则不会
  res.writeHead(200, 'success', {
    'Content-Type': 'text/html;charset=utf8'
  })
  
})

server.listen(3000)