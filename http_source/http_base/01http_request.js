const http = require('http');
const url = require('url')

const server = http.createServer();
// curl -v -d zb=213 -X POST http://localhost:3000/user?id=4#top
server.on('connection', function() {
  console.log('connection')
})

server.on('request', function(req, res) {
  console.log('req.url', req.url)
  console.log('req.method', req.method)
  let { pathname, query } = url.parse(req.url, true);
  console.log('pathname', pathname)
  console.log('query', query)
  console.log('req.headers', req.headers)
  let result = []
  req.on('data', function(data){
    result.push(data)
  })
  req.on('end', function() {
    let r = Buffer.concat(result);
    console.log('r.toString()', r.toString())
    res.end(r);
  })
})

server.on('close', function(){
  console.log('server close');
})
server.listen(3000)