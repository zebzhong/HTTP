const http = require('http')
const fs = require('fs')

const app = http.createServer((req, res) => {
  console.log("req come：" + req.url);

  if(req.url === '/') {
    // 301表示永久重定向，第一次会有两个请求（/和/new），后续服务端则只会有一个/new请求，浏览器会尽可能长的缓存/这个请求，如果这个时候把返回改成200，浏览器依然会返回/new的数据，这个时候已经由客户决定了，除非用户主动清空缓存，否则后续会一直跳转到/new
    res.writeHead(301, {
      'Location': '/new'
    })
    res.writeHead(200)
    res.end()
  }
  // if(req.url === '/') {
  //   // 302表示临时重定向，如果请求到了/，有302和Location，则会重新发起到location的请求，即每次服务端都会有两个请求
  //   res.writeHead(302, {
  //     'Location': '/new'
  //   })
  //   res.end()
  // }
  if(req.url === '/new') {
    res.writeHead(200, {
      'Content-Type': 'text/html'
    })
    res.end('<div>this is new Content</div>')
  }
  
})

app.listen(3000)