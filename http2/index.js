const http2 = require('http2'); 
const fs = require('fs');
const path = require('path');
const { HTTP2_HEADER_PATH, HTTP2_HEADER_STATUS } = http2.constants

const staticDir = path.resolve(__dirname, './public');

const options = { 
  key: fs.readFileSync(path.resolve(__dirname, './server.key')), 
  cert: fs.readFileSync(path.resolve(__dirname, './server.crt')), 
};

const server = http2.createSecureServer(options); 

server.on('stream', async (stream, headers) => {
  let reqPath = headers[HTTP2_HEADER_PATH];
  console.log('reqPath', reqPath)
  if(['/', '/index.html', '/index'].includes(reqPath)) {
    reqPath = '/index.html';
    let dirs = fs.readdirSync(staticDir);
    console.log('dirs', dirs)
    dirs.forEach(dir => {
      let pushPath = path.join(staticDir, dir);
      console.log('pushPath', pushPath)
      stream.pushStream({ [HTTP2_HEADER_PATH]: '/' + dir }, (err, pushStream) => {
        fs.createReadStream(pushPath).pipe(pushStream)
      })
    })
    stream.respondWithFile(path.join(__dirname, reqPath), {
      'Content-Type': 'text/html;charset=utf8'
    })
  } else {
    stream.respond({
      [HTTP2_HEADER_STATUS]: 404
    })
    stream.end('Not Found')
  }
})

server.on('error', (err) => console.error(err));

server.listen(3000, (err) => {
  console.log('server started at port 3000')
})