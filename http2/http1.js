let http = require('http');
let path = require('path');
let fs = require('fs');

let staticDir = path.resolve(__dirname, './public');

const server = http.createServer((req, res) => {
  let reqPath = req.url;
  if(reqPath.includes('favicon.ico')) {
    res.statusCode = 404;
    res.end('Not Found');
    return;
  }
  if(['/', '/index.html', '/index'].includes(reqPath)) {
    fs.createReadStream(path.resolve(__dirname, './index.html')).pipe(res);
  } else {
    fs.createReadStream(path.join(staticDir, '.', reqPath)).pipe(res);
  }
})

server.listen(3001, () => {
  console.log('http1.1 server started at 3001');
})