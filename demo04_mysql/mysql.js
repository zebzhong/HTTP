(async () => {
  const mysql = require('mysql2/promise')
  // 链接配置
  const cfg = {
    host: 'localhost',
    user: 'root',
    password: "123456",
    database: 'nodemysql'
  }

  const conn = await mysql.createConnection(cfg)

  let res = await conn.execute(`
    CREATE TABLE IF NOT EXISTS test(
      id INT NOT NULL AUTO_INCREMENT,
      message VARCHAR(45) NULL,
      PRIMARY KEY (id)
    )
  `)
	let insertRet = await conn.execute(`INSERT INTO test(message) VALUES(?)`, ['abc']);
	console.log(insertRet);
	let [rows, field] = await conn.execute(`SELECT * FROM  test`);
	console.log(rows);
	
})()