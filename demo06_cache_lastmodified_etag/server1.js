const fs = require('fs')
const http = require('http')

const app = http.createServer((req, res) => {
  console.log(req.url);

  if(req.url === '/') {
    const html = fs.readFileSync('test.html', 'utf8')
    res.writeHead(200, {
      'Content-Type': 'text/html'
    })
    res.end(html)
  }

  if(req.url === '/script.js') {
    res.writeHead(200, {
      'Content-Type': 'text/javascript',
      // 设置此选项，则表示20s内不会在去请求，而是会读缓存，服务器也不在会收到此请求，但是如果服务端这个时候针对数据调整了，例如修改了某个资源文件(js，css...)，客户端也不会在去请求，这个时候前端一般会在资源后面添加hash去防止资源的重复，这样更新就可以直接访问到了
      'Cache-Control': 'max-age=20'
    })
    res.end('console.log("script end twice")');
  }

  if(req.url === 'favicon.ico') {
    res.end('')
  }
})

app.listen(3000)